﻿using EmployeesTestAPI.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Factory
{
    class MonthlySalaryEmployeeFactory : EmployeeFactory
    {
        private int _id;
        private string _name;
        private int _roleId;
        private string _roleName;
        private string _roleDescription;
        private int _hourlySalary;
        private int _monthlySalary;
        private int _annualSalary;

        public MonthlySalaryEmployeeFactory(int id, string name, int roleId, string roleName, string roleDescription, int hourlySalary, int monthlySalary)
        {
            _id = id;
            _name = name;
            _roleId = roleId;
            _roleName = roleName;
            _roleDescription = roleDescription;
            _hourlySalary = hourlySalary;
            _monthlySalary = monthlySalary;
            //_annualSalary = annualSalary;
        }

        public override EmployeeDto GetEmployee()
        {
            return new MonthlySalaryEmployee(_id, _name, _roleId, _roleName, _roleDescription, _hourlySalary, _monthlySalary);
        }
    }
}
