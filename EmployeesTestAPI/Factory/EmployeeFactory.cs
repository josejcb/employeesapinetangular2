﻿using EmployeesTestAPI.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Factory
{
    public abstract class EmployeeFactory
    {
        public abstract EmployeeDto GetEmployee();
    }
}
