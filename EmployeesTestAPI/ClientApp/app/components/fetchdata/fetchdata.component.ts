import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'fetchdata',
    templateUrl: './fetchdata.component.html'
})
export class FetchDataComponent {
    public data: Employees[];

    constructor(public http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/SampleData/GetEmployees/0').subscribe(result => {
            this.data = result.json() as Employees[];
        }, error => console.error(error));
    }

    search(id: any) {  
        if (id == null || id == "" || id == undefined)
            id = 0;
        let URL = "http://localhost:58501/api/sampledata/getemployees/" + id;        
            this.http.get(URL)
                .subscribe(result => {
                    this.data = result.json() as Employees[];
                }, error => console.error(error));
    }
}

interface Employees {
    id: number;
    name: string;
    roleId: number;
    roleName: string;
    roleDescription: string;
    contractTypeName: string;
    hourlySalary: number;
    monthlySalary: number;
    annualSalary: number;
}
