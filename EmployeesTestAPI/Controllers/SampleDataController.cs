using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeesTestAPI.Dto;
using EmployeesTestAPI.Factory;
using EmployeesTestAPI.Repository;
using EmployeesTestAPI.Service;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace EmployeesTestAPI.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private IEmployeeService _employeesService;
        private EmployeesRepo _employeesRepo;
        public SampleDataController(IEmployeeService employeesService, EmployeesRepo employeesRepo)
        {
            _employeesService = employeesService;
            _employeesRepo = employeesRepo;
        }

        [HttpGet("[action]/{id}")]
        public async Task<List<EmployeeDto>> GetEmployees(int id)
        {
            try
            {
                //Get data from service
                var employees = await _employeesService.GetEmployees();
                if(id == 0)
                    return _employeesRepo.GetAll(employees);
                else
                    return _employeesRepo.GetEmployeeById(employees, id);
            }
            catch (Exception ex) { return null; }
        }
    }
}
