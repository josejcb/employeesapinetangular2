﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Service
{
    public class EmployeesService: IEmployeeService
    {            
        public async Task<object> GetEmployees()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("accept", "application/json");
                var response = await client.GetAsync("http://masglobaltestapi.azurewebsites.net/api/Employees");
                return response.Content.ReadAsStringAsync().Result;
            }
        }    
    }
}
