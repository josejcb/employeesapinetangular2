﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Service
{
    public interface IEmployeeService
    {
        Task<object> GetEmployees();        
    }
}
