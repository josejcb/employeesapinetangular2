﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Dto
{
    public abstract class EmployeeDto
    {        
        public abstract int Id { get; set; }
        public abstract string Name { get; set; }
        public abstract string ContractTypeName { get; }
        public abstract int RoleId { get; set; }
        public abstract string RoleName { get; set; }
        public abstract string RoleDescription { get; set; }
        public abstract int HourlySalary { get; set; }
        public abstract int MonthlySalary { get; set; }
        public abstract int AnnualSalary { get; }
    }
}
