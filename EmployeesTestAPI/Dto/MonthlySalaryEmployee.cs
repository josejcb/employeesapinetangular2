﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Dto
{
    public class MonthlySalaryEmployee : EmployeeDto
    {
        private int _id;
        private string _name;
        private readonly string _contractTypeName;
        private int _roleId ;
        private string _roleName;
        private string _roleDescription;
        private int _hourlySalary;
        private int _monthlySalary;
        private int _annualSalary;

        public MonthlySalaryEmployee(int id, string name, int roleId, string roleName, string roleDescription, int hourlySalary, int monthlySalary)
        {
            _contractTypeName = "MonthlySalaryEmployee";
            _id = id;
            _name = name;
            _roleId = roleId;
            _roleName = roleName;
            _roleDescription = roleDescription;
            _hourlySalary = hourlySalary;
            _monthlySalary = monthlySalary;
        }

        public override string ContractTypeName
        {
            get { return _contractTypeName; }
        }

        public override int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public override string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override int RoleId
        {
            get { return _roleId; }
            set { _roleId = value; }
        }

        public override string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }

        public override string RoleDescription
        {
            get { return _roleDescription; }
            set { _roleDescription = value; }
        }

        public override int HourlySalary
        {
            get { return _hourlySalary; }
            set { _hourlySalary = value; }
        }

        public override int MonthlySalary
        {
            get { return _monthlySalary; }
            set { _monthlySalary = value; }
        }

        public override int AnnualSalary
        {
            get { return _monthlySalary * 12; }
        }
    }
}
