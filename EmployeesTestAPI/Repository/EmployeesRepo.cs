﻿using EmployeesTestAPI.Dto;
using EmployeesTestAPI.Factory;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeesTestAPI.Repository
{
    public class EmployeesRepo
    {

        public List<EmployeeDto> GetAll(object employees)
        {
            JArray response = JArray.Parse((string)employees);
            List<EmployeeDto> employeesList = new List<EmployeeDto>();
            foreach (var item in response)
            {
                employeesList.Add(Factoring(item).GetEmployee());
            }
            return employeesList;
        }

        public List<EmployeeDto> GetEmployeeById(object employees, int id)
        {            
            JArray response = JArray.Parse((string)employees);
            List<EmployeeDto> employeesList = new List<EmployeeDto>();
            foreach (var item in response)
            {
                if ((int)item["id"] == id)
                {
                    employeesList.Add(Factoring(item).GetEmployee());
                }
            }
            return employeesList;
        }

        private EmployeeFactory Factoring(JToken item)
        {
            //Cataloging data in a list depending on salary in factory objects
            switch (item["contractTypeName"].ToString())
            {
                case "MonthlySalaryEmployee":
                    return new MonthlySalaryEmployeeFactory((int)item["id"], item["name"].ToString(), (int)item["roleId"],
                        item["roleName"].ToString(), item["roleDescription"].ToString(), (int)item["hourlySalary"], (int)item["monthlySalary"]);
                case "HourlySalaryEmployee":
                    return new HourlySalaryEmployeeFactory((int)item["id"], item["name"].ToString(), (int)item["roleId"],
                        item["roleName"].ToString(), item["roleDescription"].ToString(), (int)item["hourlySalary"], (int)item["monthlySalary"]);
                default:
                    return null;
            }
        }
    }
}
