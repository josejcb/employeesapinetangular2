﻿using EmployeesTestAPI.Controllers;
using EmployeesTestAPI.Repository;
using EmployeesTestAPI.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace XUnitTestEmployeesAPI
{
    public class EmployeesControllerTest
    {
        SampleDataController _controller;
        EmployeesRepo _repo;
        IEmployeeService _service;

            public EmployeesControllerTest()
            {
                _service = new EmployeesServiceFake();
                _repo = new EmployeesRepo();
                _controller = new SampleDataController(_service, _repo);
            }

            [Fact]
            public void Get_WhenCalled_ReturnsOkResult()
            {
                // Act
                var okResult = _controller.GetEmployees(0);

                // Assert
                Assert.IsType<OkObjectResult>(okResult.Result);
            }

            [Fact]
            public void Get_WhenCalled_ReturnsAllItems()
            {
            // Act
            var okResult = _controller.GetEmployees(0);

                // Assert
                var items = Assert.IsType<List<EmployeeDtoFake>>(okResult.Result);
                Assert.Equal(2, items.Count);
            }
    }
}
