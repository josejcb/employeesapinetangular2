using EmployeesTestAPI.Dto;
using EmployeesTestAPI.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestEmployeesAPI
{
    public class EmployeesServiceFake: IEmployeeService
    {
        private readonly Task<object> _employeesList;

        public EmployeesServiceFake()
        {
            var tcs = new TaskCompletionSource<object>();
            tcs.SetResult((new List<EmployeeDtoFake>() {
                new EmployeeDtoFake() { Id = 1, Name= "Juan", ContractTypeName= "HourlySalaryEmployee", RoleId= 1, RoleName= "Administrator", RoleDescription= null, HourlySalary= 60000, MonthlySalary= 80000 },
                new EmployeeDtoFake() { Id = 2, Name= "Sebastian", ContractTypeName= "MonthlySalaryEmployee", RoleId= 2, RoleName= "Contractor", RoleDescription= null, HourlySalary= 60000, MonthlySalary= 80000 }
            }));
            _employeesList = tcs.Task;
        }

        public Task<object> GetEmployees()
        {
            return _employeesList;
        }
    }
}
